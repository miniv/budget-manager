# Budget manager

Mentorship app which allows you to manage your budget and watch spendings statistics.
You can create your own spending categories.
You can add your incomes and spending assigned to different categories.
You can watch you spendings statistics for specific dates, grouped by categories, sotred by date or value.


#### Todos
Overall this should be a RESTful API with few models: user, category, spending*. Use mongodb as a main database. All requests should be validated before processing. In addition you may implement cashing system

*you may choose names whatever you like to

- Authorization. Implement token based authorization
User should be able to register and authorize himself using any basic credentials.
User shoud be able to authorize using Google or Facebook
 
- Categories
There should some default cattegories.
User can create his own categories as well

- Spendings
User should be able to add spendings assigned to some category. In addition you may implement possibility to attach receipt image

- Statistics
User can watch his spendings statystics for specific period of time (today, month, year..): total spendings, spendings per category. He should be able to sort the result

- Test
Create unit or integration tests for the api


#### Packages suggestions:
  - *npm* or *yarn* as package manager
  - *express* for REST api implementation
  - *passport* for auth
  - *mocha, chai, sinon* for tests
  - *mongoose* for database connection
  - *joi* for validation
